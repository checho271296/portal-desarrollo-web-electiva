var map = L.map("map", {
  center: [10.0, 5.0],
  minZoom: 2,
  zoom: 2
}).setView([9.9146, -84.0504], 14);

L.tileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
  attribution:
    '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
  subdomains: ["a", "b", "c"]
}).addTo(map);

var myURL = jQuery('script[src$="leaf-demo.js"]')
  .attr("src")
  .replace("leaf-demo.js", "");

L.tileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
  attribution:
    '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
  subdomains: ["a", "b", "c"]
}).addTo(map);

var myURL = jQuery('script[src$="leaf-demo.js"]')
  .attr("src")
  .replace("leaf-demo.js", "");

var myIcon = L.icon({
  iconUrl: myURL + "bar.jpg",
  iconSize: [29, 24],
  iconAnchor: [9, 21],
  popupAnchor: [0, -14]
});

var myIcon2 = L.icon({
  iconUrl: myURL + "bull.jpg",
  iconSize: [29, 24],
  iconAnchor: [9, 21],
  popupAnchor: [0, -14]
});

var myIcon3 = L.icon({
  iconUrl: myURL + "river.png",
  iconSize: [29, 24],
  iconAnchor: [9, 21],
  popupAnchor: [0, -14]
});
var myIcon4 = L.icon({
  iconUrl: myURL + "mountain.jpeg",
  iconSize: [29, 24],
  iconAnchor: [9, 21],
  popupAnchor: [0, -14]
});
var myIcon5 = L.icon({
  iconUrl: myURL + "church.jpg",
  iconSize: [29, 24],
  iconAnchor: [9, 21],
  popupAnchor: [0, -14]
});

var markerClusters = L.markerClusterGroup();
var flag = 0;
for (var i = 0; i < markers.length; ++i) {
  var popup =
    markers[i].name +
    "<br/>" +
    markers[i].city +
    Math.round(markers[i].alt * 0.3048);
  if (flag == 0) {
    var m = L.marker([markers[i].lat, markers[i].lng], {
      icon: myIcon
    }).bindPopup(popup);
  }
  if (flag == 1) {
    var m = L.marker([markers[i].lat, markers[i].lng], {
      icon: myIcon2
    }).bindPopup(popup);
  }
  if (flag == 2) {
    var m = L.marker([markers[i].lat, markers[i].lng], {
      icon: myIcon3
    }).bindPopup(popup);
  }
  if (flag == 3) {
    var m = L.marker([markers[i].lat, markers[i].lng], {
      icon: myIcon4
    }).bindPopup(popup);
  }
  if (flag == 4) {
    var m = L.marker([markers[i].lat, markers[i].lng], {
      icon: myIcon5
    }).bindPopup(popup);
  }
  flag = flag + 1;
  markerClusters.addLayer(m);
}
map.addLayer(markerClusters);
