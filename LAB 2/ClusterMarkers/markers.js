var markers = [
  {
    name: "Curridabat",
    city: "Curridabat, San Jose",
    lat: 9.9145,
    lng: -84.0379
  },
  {
    name: "Zapote",
    city: "Zapote, San Jose",
    lat: 9.9213,
    lng: -84.0597
  },
  {
    name: "San Francisco de Dos Rios",
    city: "San Francisco de Dos Rios, San Jose",
    lat: 9.9084,
    lng: -84.0595
  },
  {
    name: "San Diego",
    city: "San Diego, Cartago",
    lat: 9.9001,
    lng: -84.0034
  },
  {
    name: "Desamparados",
    city: "Desamparados, Cartago",
    lat: 9.8976,
    lng: -84.0665
  }
];
