var map = L.map("map").setView([9.90026, -84.01812], 14);

L.tileLayer(
  "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}",
  {
    attribution:
      'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: "mapbox/streets-v11",
    accessToken:
      "pk.eyJ1IjoiY2hlY2hvMjcxMiIsImEiOiJjazU0bnZ5YW4wbDlyM2pyNWh2aWwzNHliIn0.i7NIAK7ybRuLYmWmv3vOWQ"
  }
).addTo(map);

var banderaCarrera = L.icon({
  iconUrl: "banderaCarrera.svg",

  iconSize: [100, 100], // size of the icon
  shadowSize: [50, 64], // size of the shadow
  iconAnchor: [50, 94], // point of the icon which will correspond to marker's location
  shadowAnchor: [4, 62], // the same for the shadow
  popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
});

L.marker([9.90014, -84.01511], { icon: banderaCarrera }).addTo(map);

var message = [
  "Punto de Partida y Llegada",
  "Doblar a la derecha",
  "Punto de Hidratacion",
  "Doblar a la derecha",
  "Doblar a la izquierda",
  "Doblar a la derecha",
  "Punto de hidratacion",
  "Doblar a la derecha",
  "Doblar a la derecha",
  "Doblar a la derecha",
  "Punto de Partida y Llegada"
];

const xhttp = new XMLHttpRequest();

xhttp.open("GET", "mydata.json", true);

xhttp.send();

xhttp.onreadystatechange = function() {
  var antx;
  var anty;
  var camino = 0;
  if (this.readyState == 4 && this.status == 200) {
    let datos = JSON.parse(this.responseText);
    var flag = 0;
    // let res = document.querySelector('')
    for (let item of datos) {
      var latlng = L.latLng({
        lat: item.latitud,
        lng: item.longitud
      });
      var circle = L.circle(latlng, {
        color: "blue",
        fillColor: "#f03",
        fillOpacity: 0.5,
        radius: 40
      })
        .addTo(map)
        .bindPopup(message[camino]);

      L.Routing.control({
        waypoints: [
          L.latLng(antx, anty),
          L.latLng(item.latitud, item.longitud)
        ],
        routeWhileDragging: true,
        createMarker: function() {
          return null;
        }
      }).addTo(map);
      antx = item.latitud;
      anty = item.longitud;
      camino = camino + 1;
    }
  }
};
