create database LoginWeb

use LoginWeb

CREATE TABLE IF NOT EXISTS Usuario (
    userName VARCHAR(30)  PRIMARY KEY,
    nameUSer VARCHAR(30) NOT NULL,
    lastName1 VARCHAR(30) NOT NULL,
    lastName2 VARCHAR(30) NOT NULL,
    startDate DATE NOT NULL,
    phone INT NOT NULL,
    mail VARCHAR(30) NOT NULL,
    passwUser VARCHAR(30) NOT NULL
)